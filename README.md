# Official documentation
* [*Release the Fedora Container Base Image*
  ](https://docs.pagure.org/releng/sop_release_container_base_image.html)
---
* [*Build smaller containers*
  ](https://fedoramagazine.org/build-smaller-containers/)
  2021-04 Daniel Schier	(Fedora Magazine)
* [*How to build Fedora container images*
  ](https://fedoramagazine.org/how-to-build-fedora-container-images/)
  2019-09 Clément Verna	(Fedora Magazine)
* [*Building Smaller Container Images*
  ](https://fedoramagazine.org/building-smaller-container-images/)
  2019-05 Muayyad Alsadi (Fedora Magazine)

# For Flatpak see:
* https://gitlab.com/debian-packages-demo/flatpak

# Live ISO images (CD/DVD/...)
* [*How to boot from non-Ubuntu live ISO images like Fedora or CentOS?
  *](https://askubuntu.com/questions/141940/how-to-boot-from-non-ubuntu-live-iso-images-like-fedora-or-centos)
  ~2018 [Some ideas to try. But `iso-scan/filename=` works now in 2020.]
* [*Creating and using a live installation image*
  ](https://docs.fedoraproject.org/en-US/quick-docs/creating-and-using-a-live-installation-image/#proc_creating-and-using-live-cd)
  (2020) Fedora Docs
* [*Netboot a Fedora Live CD*
  ](https://fedoramagazine.org/netboot-a-fedora-live-cd/)
  2019-02 	Gregory Bartholomew

# Runit on Fedora
## Using Busybox
* [*runit is not packaged on fedora.*
  ](https://gitlab.com/gitlab-org/gitlab-development-kit/issues/496)
  (2019)
* [*Runit*](https://wiki.archlinux.org/index.php/Runit)

## Contributed Runit RPM
* RPM Sphere
  * https://repology.org/project/runit/versions
